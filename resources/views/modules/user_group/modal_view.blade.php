<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail User Group</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body m-3">
                <div class="loader">
                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                </div>

                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

@push('component_js')
    <script>
        const MODAL_VIEW = new bootstrap.Modal($('#modal_view'));

        function viewData(id) {
            $('#modal_view .data').hide();
            $('#modal_view .loader').fadeIn();

            $("#modal_view .data").html('');

            MODAL_VIEW.show();

            $.ajax({
                url: '{{ env('APP_URL') }}/user-group/' + id,
                method: 'GET',
                success: function(response) {
                    if (response.status == 'success') {
                        let data = response.data;
                        $('#modal_view .data').html(`
                            <div class="mb-3">
                                <label class="from-label">ID User Group</label>
                                <input class="form-control" type="text" value="${data.id_user_group}" disabled>
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Nama User Group</label>
                                <input class="form-control" type="text" value="${data.name}" disabled>
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Dibuat Oleh</label>
                                <input class="form-control" type="text" value="${data.created_by?.name ? data.created_by.name : '-'}" disabled>
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Dibuat Pada</label>
                                <input class="form-control" type="text" value="${data.created_at ? moment(data.created_at).format('DD/MM/YYYY') : '-'}" disabled>
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Diperbarui Oleh</label>
                                <input class="form-control" type="text" value="${data.updated_by?.name ? data.updated_by.name : '-'}" disabled>
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Diperbarui Pada</label>
                                <input class="form-control" type="text" value="${data.updated_at ? moment(data.updated_at).format('DD/MM/YYYY') : '-'}" disabled>
                            </div>
                            <span class="d-block text-primary mt-5 mb-3">Hak Akses</span>
                        `);

                        // PARSE PRIVILEGES

                        data.privileges.forEach(function(el) {
                            $('#modal_view .data').append(`
                                <div class="mb-3">
                                    <label class="d-block form-label"><strong>${el.menu_name}</strong></label>
                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input class="form-check-input" type="checkbox" disabled ${el.has_view == 1 && 'checked'}>
                                        <label class="form-check-label">View</label>
                                    </div>

                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input class="form-check-input" type="checkbox" disabled ${el.has_create == 1 && 'checked'}>
                                        <label class="form-check-label">Create</label>
                                    </div>

                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input class="form-check-input" type="checkbox" disabled ${el.has_edit == 1 && 'checked'}>
                                        <label class="form-check-label">Edit</label>
                                    </div>

                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input class="form-check-input" type="checkbox" disabled ${el.has_delete == 1 && 'checked'}>
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                    <hr />
                                </div>
                            `);
                        })

                        $('#modal_view .data').fadeIn();
                        $('#modal_view .loader').hide();
                    } else {
                        MODAL_VIEW.hide();
                        if (TBL_USER_GROUP) {
                            TBL_USER_GROUP.ajax.reload(null, false);
                        }
                        window.notyf.open({
                            type: response.status,
                            message: response.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });
                    }
                },
                error: function() {
                    MODAL_VIEW.hide();
                    if (TBL_USER_GROUP) {
                        TBL_USER_GROUP.ajax.reload(null, false);
                    }
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            })

        }

        $('#modal_view').on('hidden.bs.modal', function() {
            $("#modal_view .data").html('');
        });
    </script>
@endpush
