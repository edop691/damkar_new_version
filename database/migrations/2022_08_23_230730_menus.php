<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Menus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->uuid('id_menu')->primary();
            $table->string('name')->nullable();
            $table->string('icon')->nullable();
            $table->uuid('id_menu_group')->nullable();
            $table->string('url')->nullable();
            $table->integer('sort')->nullable();
            $table->integer('is_hidden')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
