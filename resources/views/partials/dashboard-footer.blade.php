<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-start">
                <p class="mb-0">
                    <strong>Dinas Pemadam Kebakaran Kab. Bogor</strong> <strong> 2023</strong>
                </p>
            </div>
        </div>
    </div>
</footer>
