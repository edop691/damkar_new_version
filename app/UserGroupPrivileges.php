<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class UserGroupPrivileges extends Model
{
    use Uuids;

    protected $table = 'user_group_privileges';
    protected $primaryKey = 'id_user_group_privilege';

    protected $fillable = [
        'id_user_group',
        'has_view',
        'has_create',
        'has_edit',
        'has_delete'
    ];

    public $timestamps = false;
}
