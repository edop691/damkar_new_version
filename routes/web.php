<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$proxy_url    = env('PROXY_URL');
$proxy_schema = env('PROXY_SCHEMA');

if (!empty($proxy_url)) {
    URL::forceRootUrl($proxy_url);
}

if (!empty($proxy_schema)) {
    URL::forceScheme($proxy_schema);
}

Route::get('/', 'HomeController@index');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@authenticate');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dasboard');

    Route::get('/pengguna', 'UserController@index');
    Route::post('/pengguna', 'UserController@add');
    Route::delete('/pengguna', 'UserController@delete');
    Route::patch('/pengguna', 'UserController@edit');
    Route::get('/pengguna/all', 'UserController@all');
    Route::get('/pengguna/master', 'UserController@master');
    Route::get('/pengguna/{id}', 'UserController@find');

    Route::get('/user-group', 'UserGroupController@index');
    Route::post('/user-group', 'UserGroupController@add');
    Route::patch('/user-group', 'UserGroupController@edit');
    Route::delete('/user-group', 'UserGroupController@delete');
    Route::get('/user-group/master', 'UserGroupController@master');
    Route::get('/user-group/all', 'UserGroupController@all');
    Route::get('/user-group/{id}', 'UserGroupController@find');

    Route::get('/setup', 'HalamanDepanController@index');

    Route::get('/tamu', 'DaftarTamuController@index');

    Route::get('/feedback', 'KepuasanPelangganController@index');

    Route::get('/logout', 'LoginController@logout');
});
