<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserGroups extends Model
{
    use Uuids, SoftDeletes;

    protected $table = 'user_groups';
    protected $primaryKey = 'id_user_group';

    protected $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function created_by()
    {
        return $this->belongsTo('\App\User', 'created_by', 'id_user');
    }

    public function updated_by()
    {
        return $this->belongsTo('\App\User', 'updated_by', 'id_user');
    }

    public function deleted_by()
    {
        return $this->belongsTo('\App\User', 'deleted_by', 'id_user');
    }
}
