<?php

namespace App\Helpers;

use stdClass;

use App\MenuGroups;
use App\Menus;
use App\User;
use App\UserGroupPrivileges;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Session;


class PrivilegeHelper
{
    public static function createMenu()
    {
        $current_url = '/' . Request::segment(1);
        // PRIVILEGES -> FIND MENU GROUP -> FIND MENU
        //            -> FIND MENU WITHOUT GROUP MENU

        $id_user = Auth::id();

        // GET DETAIL USER
        $user = User::find($id_user);

        // GET ROLE PRIVILEGES
        $privileges = UserGroupPrivileges::where('id_user_group', '=', $user->id_user_group)
            ->where('has_view', '=', 1)
            ->select('id_menu')
            ->get()
            ->toArray();

        // GET MENU
        $menus = Menus::whereIn('id_menu', $privileges)->where('is_hidden', '=', 0)->orderBy('sort', 'ASC')->get();
        $menus = $menus->groupBy('id_menu_group');
        $menu = [];

        // GET GROUP
        foreach ($menus as $key => $mn) {

            if (!$mn[0]->id_menu_group) {
                $mn = $mn->toArray();
                for ($i = 0; $i < count($mn); $i++) {
                    $item = $mn[$i];
                    $tmp = [
                        'menus' => [$item],
                        'sort' => (int)$item['sort']
                    ];

                    array_push($menu, $tmp);
                }
            } else {
                $menu_group = MenuGroups::find($mn[0]->id_menu_group);
                $tmp = [
                    'group' => $menu_group->name,
                    'sort' => (int)$menu_group->sort,
                    'menus' => $mn->toArray()
                ];
                array_push($menu, $tmp);
            }
        }
        $menu = collect($menu)->sortBy('sort')->toArray();

        // GENERATE COMPONENT
        $comp = '';
        foreach ($menu as $ls) {
            if (!isset($ls['group'])) {
                $lsm = $ls['menus'][0];
                if ($current_url == $lsm['url']) {
                    $comp .= '<li class="sidebar-item active">
                            <a class="sidebar-link" href="' . env('APP_URL') .  $lsm['url'] . '">
                                <i class="align-middle" data-feather="' .  $lsm['icon'] . '"></i>
                                <span class="align-middle">' . $lsm['name'] . '</span>
                            </a>
                        </li>';
                } else {
                    $comp .= '<li class="sidebar-item">
                            <a class="sidebar-link" href="' . env('APP_URL') .  $lsm['url'] . '">
                                <i class="align-middle" data-feather="' .  $lsm['icon'] . '"></i>
                                <span class="align-middle">' . $lsm['name'] . '</span>
                            </a>
                        </li>';
                }
            } else {
                $comp .= '<li class="sidebar-header">' . $ls['group'] . '</li>';

                foreach ($ls['menus'] as $lsm) {
                    if ($current_url == $lsm['url']) {
                        $comp .= '<li class="sidebar-item active">
                                    <a class="sidebar-link" href="' . env('APP_URL') .  $lsm['url'] . '">
                                        <i class="align-middle" data-feather="' .  $lsm['icon'] . '"></i>
                                        <span class="align-middle">' . $lsm['name'] . '</span>
                                    </a>
                                </li>';
                    } else {
                        $comp .= '<li class="sidebar-item">
                                    <a class="sidebar-link" href="' . env('APP_URL') .  $lsm['url'] . '">
                                        <i class="align-middle" data-feather="' .  $lsm['icon'] . '"></i>
                                        <span class="align-middle">' . $lsm['name'] . '</span>
                                    </a>
                                </li>';
                    }
                }
            }
        }

        return $comp;
    }

    public static function getPermission()
    {
        $user = Auth::user();
        $current_url = '/' . Request::segment(1);
        $id_menu = Menus::where('url', '=', htmlspecialchars($current_url))->first();
        $id_menu = $id_menu->id_menu;

        $permission = UserGroupPrivileges::where('id_menu', '=', $id_menu)
            ->where('id_user_group', '=', $user->id_user_group)
            ->select(
                'has_view',
                'has_create',
                'has_edit',
                'has_delete'
            )
            ->first();

        if (!$permission) {
            $permission = new stdClass();
            $permission->has_view = 0;
            $permission->has_create = 0;
            $permission->has_edit = 0;
            $permission->has_delete = 0;
        }

        return $permission;
    }
}
