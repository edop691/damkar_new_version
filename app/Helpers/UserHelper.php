<?php

namespace App\Helpers;

use App\UserGroups;
use Illuminate\Support\Facades\Auth;

class UserHelper
{
    public static function getDisplayName()
    {
        $user = Auth::user();
        $name = $user->name;
        if (strlen($name) > 20)
            $name = substr($name, 0, 17) . '...';
        return $name;
    }

    public static function getInitialDisplayName()
    {
        $user = Auth::user();
        $name = $user->name;
        $obj_name = explode(' ', $name);
        $initial = $obj_name[0][0];
        if (count($obj_name) > 1) {
            $initial .= $obj_name[1][0];
        }

        return $initial;
    }

    public static function getGroupName()
    {
        $user = Auth::user();
        $id_group = $user->id_user_group;

        $group = UserGroups::withTrashed()->find($id_group);
        return $group->name;
    }
}
