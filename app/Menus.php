<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    use Uuids;

    protected $table = 'menus';
    protected $primaryKey = 'id_menu';

    protected $fillable = [
        'name',
        'icon',
        'id_menu_group',
        'url',
        'sort'
    ];

    public function group_menu()
    {
        return $this->belongsTo('\App\MenuGroups', 'id_menu_group', 'id_menu_group');
    }

    public $timestamps = false;
}
