<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendataanPeralatanPerusahaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendataan_peralatan_perusahaan', function (Blueprint $table) {
            $table->id('id_pendataan_peralatan');
            $table->string('id_pendataan_umum')->nullable();
            $table->string('perlengkapan_las')->nullable();
            $table->string('persediaan_bahan')->nullable();
            $table->string('ruang_generator')->nullable();
            $table->string('dinding_ruang_generator')->nullable();
            $table->string('ventilasi_generator')->nullable();
            $table->string('bahan_produksi_mudah_terbakar')->nullable();
            $table->string('penyimpanan_bahan_produksi')->nullable();
            $table->string('bangunan_gudang')->nullable();
            $table->string('penumpukan_bahan_produksi')->nullable();
            $table->string('sumber_listrik')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendataan_peralatan_perusahaan');
    }
}
