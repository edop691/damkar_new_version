<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class MenuGroups extends Model
{
    use Uuids;

    protected $table = 'menu_groups';
    protected $primaryKey = 'id_menu_group';

    protected $fillable = [
        'name',
        'sort'
    ];

    public $timestamps = false;
}
