<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form onsubmit="return doEdit(event)" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User Group</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body m-3">
                <div class="loader">
                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>
                </div>

                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary btn-submit" disabled>Simpan</button>
            </div>
        </form>
    </div>
</div>

@push('component_js')
    <script>
        const MODAL_EDIT = new bootstrap.Modal($('#modal_edit'));

        function editData(id) {
            $('#modal_edit .data').hide();
            $('#modal_edit .loader').fadeIn();

            $("#modal_edit .data").html('');
            $("#modal_edit .btn-submit").attr('disabled', true);

            MODAL_EDIT.show();

            $.ajax({
                url: '{{ env('APP_URL') }}/pengguna/' + id,
                method: 'GET',
                success: function(response) {
                    if (response.status == 'success') {
                        let data = response.data;
                        $('#modal_edit .data').html(`
                            <input type="hidden" name="id_user" value="${id}"/>
                            <div class="mb-3">
                                <label class="from-label">Nama Pengguna</label><sup class="text-danger">* Wajib diisi</sup>
                                <input class="form-control threshold" type="text" name="nama_pengguna" value="${data.name}" maxlength="255" required autocomplete="new-nama_pengguna">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Username</label><sup class="text-danger">* Wajib diisi</sup>
                                <input class="form-control threshold" type="text" name="username" value="${data.username}" maxlength="255" required autocomplete="new-nama_pengguna">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Password</label>
                                <small class="d-block text-primary">Isi jika ingin merubah Password</small>
                                <input class="form-control threshold" type="password" name="password" maxlength="100" autocomplete="new-password">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Email</label>
                                <input class="form-control threshold" type="email" name="email" value="${data.email ? data.email : ''}" maxlength="255" autocomplete="new-email">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Telepon</label>
                                <input class="form-control threshold" type="tel" name="telepon"  value="${data.phone ? data.phone : ''}" maxlength="20" autocomplete="new-telepon">
                            </div>
                        `);

                        let selection = '<option value="" selected disabled>=== PILIH USER GROUP ===</option>';

                        // PARSE MASTER GROUP
                        if (data.master_user_group) {
                            data.master_user_group.forEach(function(item) {
                                selection += `
                                    <option value="${item.id_user_group}" ${item.id_user_group == data.id_user_group && 'selected'}>${item.name}</option>
                                `
                            });
                        }

                        $('#modal_edit .data').append(`
                            <div class="mb-3">
                                <label class="from-label">User Group</label><sup class="text-danger">* Wajib dipilih</sup>
                                <select class="form-control" name="id_user_group" required>
                                    ${selection}
                                </select>
                            </div>
                        `);

                        $('.threshold').maxlength({
                            alwaysShow: true,
                            warningClass: "badge bg-primary",
                            limitReachedClass: "badge bg-danger"
                        });

                        $("#modal_edit .btn-submit").attr('disabled', false);
                        $('#modal_edit .data').fadeIn();
                        $('#modal_edit .loader').hide();
                    } else {
                        MODAL_EDIT.hide();
                        if (TBL_USER) {
                            TBL_USER.ajax.reload(null, false);
                        }
                        window.notyf.open({
                            type: response.status,
                            message: response.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });
                    }
                },
                error: function() {
                    MODAL_EDIT.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            })

        }

        $('#modal_edit').on('hidden.bs.modal', function() {
            $("#modal_edit .data").html('');
            $("#modal_edit .btn-submit").attr('disabled', true);
        });

        function doEdit(event) {
            event.preventDefault();
            $('#modal_edit form').removeClass('was-validated');
            $("#modal_edit .invalid-feedback").remove();
            $("#modal_edit .is-invalid").removeClass('is-invalid');

            let form = $('#modal_edit form');
            form.addClass('was-validated');
            if (!form[0].checkValidity()) {
                return false;
            }

            $("#modal_edit .data").hide();
            $("#modal_edit .loader").fadeIn();
            $("#modal_edit .btn-submit").attr('disabled', true);

            let fields = form.serializeArray();
            let payload = {};
            fields.forEach(function(el) {
                payload[el.name] = el.value;
            })

            $.ajax({
                url: "{{ env('APP_URL') }}/pengguna",
                method: 'PATCH',
                data: {
                    _token: '{{ csrf_token() }}',
                    ...payload
                },
                success: function(res) {
                    $('#modal_edit form').removeClass('was-validated');
                    $("#modal_edit .data").fadeIn();
                    $("#modal_edit .loader").hide();
                    $("#modal_edit .btn-submit").attr('disabled', false);

                    if (res.status == 'success') {
                        window.notyf.open({
                            type: res.status,
                            message: res.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });

                        if (TBL_USER) {
                            TBL_USER.ajax.reload(null, false);
                        }

                        MODAL_EDIT.hide();
                    } else {
                        // HANDLER ERROR FIELDS
                        if (res.errors) {
                            let fields = $("#modal_edit :input");
                            let errors = res.errors;
                            for (let element of fields) {
                                let name = $(element).attr('name');
                                if (name) {
                                    // FINDING ON ERROR SECTION
                                    if (name in errors) {
                                        errors[name].forEach(error => {
                                            let field = $(`[name='${name}']`)
                                            field.addClass('is-invalid');
                                            field.parent().append(`
                                                <div class="invalid-feedback">
                                                    ${error}
                                                </div>
                                            `);
                                        });
                                    }
                                }
                            }

                        } else {
                            MODAL_EDIT.hide();
                            window.notyf.open({
                                type: res.status,
                                message: res.msg,
                                duration: 10000,
                                ripple: true,
                                dismissible: true,
                                position: {
                                    x: "right",
                                    y: "top"
                                }
                            });

                            if (TBL_USER) {
                                TBL_USER.ajax.reload(null, false);
                            }
                        }
                    }
                },
                error: function() {
                    MODAL_EDIT.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            });

            return false; // prevent actual form being submitted
        }
    </script>
@endpush
