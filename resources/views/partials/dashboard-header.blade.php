<nav class="navbar navbar-expand navbar-light navbar-bg">
    <a class="sidebar-toggle js-sidebar-toggle">
        <i class="hamburger align-self-center"></i>
    </a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav navbar-align">
            <li class="nav-item">
                <a class="nav-icon js-fullscreen d-none d-lg-block" href="#">
                    <div class="position-relative">
                        <i class="align-middle" data-feather="maximize"></i>
                    </div>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-icon pe-md-0 dropdown-toggle user" href="#" data-bs-toggle="dropdown">
                    <div class="d-flex flex-row gap-2" style="max-width: 50vw">
                        <div class="d-flex flex-column user-detail-wrapper">
                            <strong>{{ \App\Helpers\UserHelper::getDisplayName() }}</strong>
                            <span>{{ \App\Helpers\UserHelper::getGroupName() }}</span>
                        </div>

                        <div class="avatar img-fluid rounded align-middle bg-primary" style="margin-top: -2.5px;">
                            <small class="d-block text-center w-100 h-100 text-white"
                                style="line-height: 40px;font-size: 1rem">
                                {{ \App\Helpers\UserHelper::getInitialDisplayName() }}
                            </small>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <a class="dropdown-item" href="pages-profile.html"><i class="align-middle me-1"
                            data-feather="user"></i> Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ env('APP_URL') }}/logout">Log out</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
