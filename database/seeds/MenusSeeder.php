<?php

use App\MenuGroups;
use Illuminate\Database\Seeder;
use App\Menus;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // GET ID MENU GROUP
        $groups = MenuGroups::all();
        $id_group_management = $groups[0]->id_menu_group;
        $id_group_laporan = $groups[1]->id_menu_group;

        $data = [
            [
                'name' => 'Dashboard',
                'icon' => 'monitor',
                'id_menu_group' => '',
                'url' => '/dashboard',
                'sort' => 1,
                'is_hidden' => 0
            ],
            [
                'name' => 'Pengguna',
                'icon' => 'user-check',
                'id_menu_group' => $id_group_management,
                'url' => '/pengguna',
                'sort' => 2,
                'is_hidden' => 0
            ],
            [
                'name' => 'Group Pengguna',
                'icon' => 'users',
                'id_menu_group' => $id_group_management,
                'url' => '/user-group',
                'sort' => 3,
                'is_hidden' => 0
            ],
            [
                'name' => 'Aduan Masyarakat',
                'icon' => ' bookmark',
                'id_menu_group' => $id_group_laporan,
                'url' => '/aduan_masyakat',
                'sort' => 4,
                'is_hidden' => 0
            ],
            [
                'name' => 'Aduan Panic Button',
                'icon' => 'award',
                'id_menu_group' => $id_group_laporan,
                'url' => '/panic_button',
                'sort' => 5,
                'is_hidden' => 0
            ],
            [
                'name' => 'Aduan Perusahaan',
                'icon' => 'tablet',
                'id_menu_group' => $id_group_laporan,
                'url' => '/perusahaan',
                'sort' => 6,
                'is_hidden' => 0
            ],
            [
                'name' => 'Hydrants',
                'icon' => 'file-text',
                'id_menu_group' => $id_group_laporan,
                'url' => '/hydrants',
                'sort' => 7,
                'is_hidden' => 0
            ],
            [
                'name' => 'Unit Tracker',
                'icon' => 'compass',
                'id_menu_group' => $id_group_laporan,
                'url' => '/unit_tracker',
                'sort' => 8,
                'is_hidden' => 0
            ],
        ];

        foreach ($data as $item) {
            $Data = new Menus();
            $Data->name = $item['name'];
            $Data->icon = $item['icon'];
            $Data->id_menu_group = $item['id_menu_group'];
            $Data->url = $item['url'];
            $Data->sort = $item['sort'];
            $Data->is_hidden = $item['is_hidden'];

            $Data->save();
        }
    }
}
