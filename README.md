# About
boilerplate laravel 7 

# Deployment
1. jalankan perintah ```composer update```
2. copy & rename file ```.env.example``` menjadi ```.env```
3. jalankan perintah ```php artisan key:generate```
4. sesuaikan konfigurasi aplikasi pada file ```.env```
5. Jalankan Migration dan Seeder dengan perintah ```php artisan migrate:fresh --seed```

# Optional
- master template bisa di download pada link berikut https://mitratechindonesia.co.id/master-template/adminkit-pro.zip
- untuk DB Oracle silahkan gunakan config dibawah ini dan rubah block db config pada ```.env```
```
DB_CONNECTION=oracle
DB_HOST=
DB_PORT=1521
DB_SERVICE_NAME=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```