<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form onsubmit="return doEdit(event)" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User Group</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body m-3">
                <div class="loader">
                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-3 placeholder-sm"></div>
                        <div class="col-12">
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                            <div class="placeholder col-2 placeholder-lg"></div>
                        </div>
                    </div>

                </div>

                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary btn-submit" disabled>Simpan</button>
            </div>
        </form>
    </div>
</div>

@push('component_js')
    <script>
        const MODAL_EDIT = new bootstrap.Modal($('#modal_edit'));

        function editData(id) {
            $('#modal_edit .data').hide();
            $('#modal_edit .loader').fadeIn();

            $("#modal_edit .data").html('');
            $("#modal_edit .btn-submit").attr('disabled', true);

            MODAL_EDIT.show();

            $.ajax({
                url: '{{ env('APP_URL') }}/user-group/' + id,
                method: 'GET',
                success: function(response) {
                    if (response.status == 'success') {
                        let data = response.data;
                        $('#modal_edit .data').html(`
                            <input type="hidden" class="id-user-group" value="${id}"/>
                            <div class="mb-3">
                                <label class="from-label">Nama User Group</label><sup class="text-danger">* Wajib diisi</sup>
                                <input class="form-control threshold user-group-name" type="text" value="${data.name}" maxlength="255">
                            </div>

                            <span class="d-block text-primary my-3">Hak Akses</span>
                        `);

                        // PARSE PRIVILEGES
                        data.privileges.forEach(function(el) {
                            $('#modal_edit .data').append(`
                                <div class="mb-3">
                                    <label class="d-block form-label"><strong>${el.menu_name}</strong></label>
                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input id="has_view_${el.id_menu}" class="form-check-input input-privileges" type="checkbox" data-section="has_view" data-id_menu="${el.id_menu}" ${el.has_view == 1 && 'checked'}>
                                        <label for="has_view_${el.id_menu}" class="form-check-label">View</label>
                                    </div>

                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input id="has_create_${el.id_menu}" class="form-check-input input-privileges" type="checkbox" data-section="has_create" data-id_menu="${el.id_menu}" ${el.has_create == 1 && 'checked'}>
                                        <label for="has_create_${el.id_menu}" class="form-check-label">Create</label>
                                    </div>

                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input id="has_edit_${el.id_menu}" class="form-check-input input-privileges" type="checkbox" data-section="has_edit" data-id_menu="${el.id_menu}" ${el.has_edit == 1 && 'checked'}>
                                        <label for="has_edit_${el.id_menu}" class="form-check-label">Edit</label>
                                    </div>

                                    <div class="form-check form-check-inline form-switch col-2">
                                        <input id="has_delete_${el.id_menu}" class="form-check-input input-privileges" type="checkbox" data-section="has_delete" data-id_menu="${el.id_menu}" ${el.has_delete == 1 && 'checked'}>
                                        <label for="has_delete_${el.id_menu}" class="form-check-label">Delete</label>
                                    </div>
                                    <hr />
                                </div>
                            `);
                        })

                        $('.threshold').maxlength({
                            alwaysShow: true,
                            warningClass: "badge bg-primary",
                            limitReachedClass: "badge bg-danger"
                        });

                        $("#modal_edit .btn-submit").attr('disabled', false);
                        $('#modal_edit .data').fadeIn();
                        $('#modal_edit .loader').hide();
                    } else {
                        MODAL_EDIT.hide();
                        if (TBL_USER_GROUP) {
                            TBL_USER_GROUP.ajax.reload(null, false);
                        }
                        window.notyf.open({
                            type: response.status,
                            message: response.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });
                    }
                },
                error: function() {
                    MODAL_EDIT.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            })

        }

        $('#modal_edit').on('hidden.bs.modal', function() {
            $("#modal_edit .data").html('');
            $("#modal_edit .btn-submit").attr('disabled', true);
        });

        function doEdit(event) {
            event.preventDefault();
            $("#modal_edit .data").hide();
            $("#modal_edit .loader").fadeIn();
            $("#modal_edit .btn-submit").attr('disabled', true);

            // PRIVILEGE HANDLER
            let privileges_raw = [];
            let privileges_component = $('#modal_edit .input-privileges');
            $.each(privileges_component, function(index, el) {
                let ins = $(el);
                let value = ins.is(':checked') ? 1 : 0;
                let id_menu = ins.data('id_menu');
                let section = ins.data('section');
                privileges_raw.push({
                    value,
                    id_menu,
                    section
                });
            });

            // GROUP BY DATA BY ID_MENU
            let unique_id = privileges_raw.map(function(item) {
                    return item.id_menu;
                })
                .filter(function(value, index, self) {
                    return self.indexOf(value) === index
                })

            let privileges = [];

            unique_id.forEach(function(el) {
                let data_group = privileges_raw.filter(function(item) {
                    return item.id_menu === el
                });

                let _obj = {};
                data_group.forEach(function(el) {
                    _obj[el.section] = el.value;
                    _obj['id_menu'] = el.id_menu;
                });
                privileges.push(_obj);
            });
            // PRIVILEGE HANDLER END

            let name = $("#modal_edit .user-group-name").val();
            let id_user_group = $("#modal_edit .id-user-group").val();

            $.ajax({
                url: "{{ env('APP_URL') }}/user-group",
                method: 'PATCH',
                data: {
                    _token: '{{ csrf_token() }}',
                    privileges,
                    name,
                    id_user_group
                },
                success: function(res) {
                    window.notyf.open({
                        type: res.status,
                        message: res.msg,
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });

                    if (TBL_USER_GROUP) {
                        TBL_USER_GROUP.ajax.reload(null, false);
                    }

                    MODAL_EDIT.hide();
                },
                error: function() {
                    MODAL_EDIT.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            });

            return false;
        }
    </script>
@endpush
