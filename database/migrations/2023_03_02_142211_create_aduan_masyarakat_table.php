<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAduanMasyarakatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aduan_masyarakat', function (Blueprint $table) {
            $table->id('id_aduan_masyarakat');
            $table->string('no_wa_pelapor')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->integer('kelurahan')->nullable();
            $table->integer('kecamatan')->nullable();
            $table->string('foto')->nullable();
            $table->string('foto_path')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('jam')->nullable();
            $table->string('jenis_kedaruratan')->nullable();
            $table->string('catatan_khusus')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aduan_masyarakat');
    }
}
