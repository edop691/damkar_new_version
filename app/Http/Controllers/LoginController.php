<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect('/dashboard');
        }

        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password',);

        if (Auth::attempt($credentials, boolval($request->remember))) {
            return redirect('login');
            // index method will handle redirect
        } else {
            return redirect('login')->with('error', 'Username / Password Salah');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
