<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form onsubmit="return doAdd(event)" class="modal-content needs-validation" novalidate>
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pengguna</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body m-3">
                <div class="loader">
                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                </div>

                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary btn-submit" disabled>Simpan</button>
            </div>
        </form>
    </div>
</div>

@push('component_js')
    <script>
        const MODAL_ADD = new bootstrap.Modal($('#modal_add'));

        function addData() {
            $('#modal_add .data').hide();
            $('#modal_add .loader').fadeIn();
            $("#modal_add .data").html('');
            $("#modal_add .btn-submit").attr('disabled', true);

            MODAL_ADD.show();

            $.ajax({
                url: '{{ env('APP_URL') }}/pengguna/master',
                method: 'GET',
                success: function(response) {
                    if (response.status == 'success') {
                        let data = response.data;
                        $('#modal_add .data').html(`
                            <div class="mb-3">
                                <label class="from-label">Nama Pengguna</label><sup class="text-danger">* Wajib diisi</sup>
                                <input class="form-control threshold" name="nama_pengguna" type="text" maxlength="255" required autocomplete="new-nama_pengguna">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Username</label><sup class="text-danger">* Wajib diisi</sup>
                                <input class="form-control threshold" name="username" type="text" maxlength="255" required autocomplete="new-username">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Password</label><sup class="text-danger">* Wajib diisi</sup>
                                <input class="form-control threshold" name="password" type="password" maxlength="100" required autocomplete="new-password">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Email</label>
                                <small class="d-block text-primary">Digunakan untuk fitur reset password</small>
                                <input class="form-control threshold" name="email" type="email" maxlength="255" autocomplete="new-email">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Telepon</label>
                                <small class="d-block text-primary">Digunakan untuk fitur reset password</small>
                                <small class="d-block text-primary">Gunakan Format 08xxx</small>
                                <input class="form-control threshold" name="telepon" type="tel" maxlength="20" autocomplete="new-telepon">
                            </div>
                        `);

                        let selection = '<option value="" selected disabled>=== PILIH USER GROUP ===</option>';
                        data.forEach(element => {
                            selection += `
                                <option value="${element.id_user_group}">${element.name}</option>
                            `
                        });

                        $('#modal_add .data').append(`
                            <div class="mb-3">
                                <label class="from-label">User Group</label><sup class="text-danger">* Wajib dipilih</sup>
                                <select class="form-control" name="id_user_group" required>
                                    ${selection}
                                </select>
                            </div>
                        `);

                        $('.threshold').maxlength({
                            alwaysShow: true,
                            warningClass: "badge bg-primary",
                            limitReachedClass: "badge bg-danger"
                        });

                        $("#modal_add .btn-submit").attr('disabled', false);
                        $('#modal_add .data').fadeIn();
                        $('#modal_add .loader').hide();
                    } else {
                        MODAL_ADD.hide();
                        if (TBL_USER) {
                            TBL_USER.ajax.reload(null, false);
                        }
                        window.notyf.open({
                            type: response.status,
                            message: response.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });
                    }
                },
                error: function() {
                    MODAL_ADD.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            })

        }

        $('#modal_add').on('hidden.bs.modal', function() {
            $("#modal_add .data").html('');
            $('#modal_add form').removeClass('was-validated');
            $("#modal_add .btn-submit").attr('disabled', true);
            $("#modal_add .invalid-feedback").remove();
        });

        function doAdd(event) {
            event.preventDefault();
            $('#modal_add form').removeClass('was-validated');
            $("#modal_add .invalid-feedback").remove();
            $("#modal_add .is-invalid").removeClass('is-invalid');

            let form = $('#modal_add form');
            form.addClass('was-validated');
            if (!form[0].checkValidity()) {
                return false;
            }

            $("#modal_add .data").hide();
            $("#modal_add .loader").fadeIn();
            $("#modal_add .btn-submit").attr('disabled', true);

            let fields = form.serializeArray();
            let payload = {};
            fields.forEach(function(el) {
                payload[el.name] = el.value;
            })

            $.ajax({
                url: "{{ env('APP_URL') }}/pengguna",
                method: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    ...payload
                },
                success: function(res) {
                    $('#modal_add form').removeClass('was-validated');
                    $("#modal_add .data").fadeIn();
                    $("#modal_add .loader").hide();
                    $("#modal_add .btn-submit").attr('disabled', false);

                    if (res.status == 'success') {
                        window.notyf.open({
                            type: res.status,
                            message: res.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });

                        if (TBL_USER) {
                            TBL_USER.ajax.reload(null, false);
                        }

                        MODAL_ADD.hide();
                    } else {
                        // HANDLER ERROR FIELDS
                        if (res.errors) {
                            let fields = $("#modal_add :input");
                            let errors = res.errors;
                            for (let element of fields) {
                                let name = $(element).attr('name');
                                if (name) {
                                    // FINDING ON ERROR SECTION
                                    if (name in errors) {
                                        errors[name].forEach(error => {
                                            let field = $(`[name='${name}']`)
                                            field.addClass('is-invalid');
                                            field.parent().append(`
                                                <div class="invalid-feedback">
                                                    ${error}
                                                </div>
                                            `);
                                        });
                                    }
                                }
                            }

                        } else {
                            MODAL_ADD.hide();
                            window.notyf.open({
                                type: res.status,
                                message: res.msg,
                                duration: 10000,
                                ripple: true,
                                dismissible: true,
                                position: {
                                    x: "right",
                                    y: "top"
                                }
                            });

                            if (TBL_USER) {
                                TBL_USER.ajax.reload(null, false);
                            }
                        }
                    }
                },
                error: function() {
                    MODAL_ADD.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            });

            return false; // prevent actual form being submitted
        }
    </script>
@endpush
