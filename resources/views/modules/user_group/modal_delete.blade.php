<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus User Group</h5>
                <input type="hidden" class="id-user-group" value="" />
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body m-3">
                <p class="mb-0 text-center"><strong>Apakah anda yakin ingin menghapus?</strong></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-danger btn-submit" onclick="doDelete()" disabled>Hapus</button>
            </div>
        </div>
    </div>
</div>

@push('component_js')
    <script>
        const MODAL_DELETE = new bootstrap.Modal($('#modal_delete'));

        function deleteData(id) {
            $("#modal_delete .btn-submit").attr('disabled', false);
            $("#modal_delete .btn-submit").html('Hapus');
            $("#modal_delete .id-user-group").val(id);
            MODAL_DELETE.show();

        }

        $('#modal_delete').on('hidden.bs.modal', function() {
            $("#modal_delete .btn-submit").attr('disabled', false);
            $("#modal_delete .btn-submit").html('Hapus');
            $("#modal_delete .id-user-group").val("");
        });

        function doDelete() {
            let id = $("#modal_delete .id-user-group").val();
            $("#modal_delete .btn-submit").html(`
                <div class="spinner-border spinner-border-sm text-white" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            `);
            $("#modal_delete .btn-submit").attr('disabled', true);

            $.ajax({
                url: "{{ env('APP_URL') }}/user-group/",
                data: {
                    id,
                    _token: "{{ csrf_token() }}"
                },
                method: 'DELETE',
                success: function(res) {
                    if (TBL_USER_GROUP) {
                        TBL_USER_GROUP.ajax.reload(null, false);
                    }
                    MODAL_DELETE.hide();
                    window.notyf.open({
                        type: res.status,
                        message: res.msg,
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                },
                error: function() {
                    MODAL_DELETE.hide();
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            })
        }
    </script>
@endpush
