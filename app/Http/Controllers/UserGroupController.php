<?php

namespace App\Http\Controllers;

use App\Helpers\PrivilegeHelper;
use App\Menus;
use App\UserGroupPrivileges;
use App\UserGroups;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use stdClass;

class UserGroupController extends Controller
{
    public function index()
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            abort(403);
        }

        return view('modules.user_group.index', ['permission' => $permission]);
    }

    public function all()
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $data = UserGroups::with([
            'created_by' => function ($query) {
                return $query->select('id_user', 'name');
            },
            'updated_by'  => function ($query) {
                return $query->select('id_user', 'name');
            }
        ])->get();

        return Datatables::collection($data)->make(true);
    }

    public function master()
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        // GET ALL MENU
        $privileges = [];
        $menus = Menus::select('name', 'id_menu', 'id_menu_group')->with('group_menu:id_menu_group,name')->get();
        foreach ($menus as $menu) {
            $obj_privilege = new stdClass();

            $obj_privilege->menu_name = "";
            if ($menu->group_menu !== null) {
                $obj_privilege->menu_name = $menu->group_menu->name;
            }

            $obj_privilege->menu_name .= ' ' . $menu->name;
            $obj_privilege->id_menu = $menu->id_menu;
            $obj_privilege->has_view = 0;
            $obj_privilege->has_create = 0;
            $obj_privilege->has_edit = 0;
            $obj_privilege->has_delete = 0;

            array_push($privileges, $obj_privilege);
        }

        return response()->json([
            'status' => 'success',
            'data' => $privileges
        ]);
    }

    public function find(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'uuid'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'msg' => 'invalid parameter'
            ]);
        }

        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $user_group = UserGroups::withTrashed()
            ->where('id_user_group', $request->id)
            ->with([
                'created_by' => function ($query) {
                    return $query->select('id_user', 'name');
                },
                'updated_by'  => function ($query) {
                    return $query->select('id_user', 'name');
                }
            ])
            ->first();

        if (!$user_group) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Data tidak ditemukan'
            ]);
        }

        // GET ALL MENU
        $privileges = [];
        $menus = Menus::select('name', 'id_menu', 'id_menu_group')->with('group_menu:id_menu_group,name')->get();
        foreach ($menus as $menu) {
            // GET PRIVILEGES
            $privilege = UserGroupPrivileges::where('id_user_group', $user_group->id_user_group)
                ->where('id_menu', $menu->id_menu)
                ->first();

            $obj_privilege = new stdClass();

            $obj_privilege->menu_name = "";
            if ($menu->group_menu !== null) {
                $obj_privilege->menu_name = $menu->group_menu->name;
            }

            $obj_privilege->menu_name .= ' ' . $menu->name;
            $obj_privilege->id_menu = $menu->id_menu;

            if ($privilege) {
                $obj_privilege->has_view = (int)$privilege->has_view;
                $obj_privilege->has_create = (int)$privilege->has_create;
                $obj_privilege->has_edit = (int)$privilege->has_edit;
                $obj_privilege->has_delete = (int)$privilege->has_delete;
            } else {
                $obj_privilege->has_view = 0;
                $obj_privilege->has_create = 0;
                $obj_privilege->has_edit = 0;
                $obj_privilege->has_delete = 0;
            }

            array_push($privileges, $obj_privilege);
        }

        $user_group->privileges = $privileges;

        return response()->json([
            'status' => 'success',
            'data' => $user_group
        ]);
    }

    public function add(Request $request)
    {
        if ($request->name && $request->privileges) {
            $permission = PrivilegeHelper::getPermission();
            if (!$permission->has_create) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
                ]);
            }

            // SAVE TO USER_GROUPS
            $User_group = new UserGroups();
            $User_group->name = $request->name;
            $User_group->created_by = Auth::id();

            if ($User_group->save()) {
                // SAVE TO USER_GROUP_PRIVILEGES
                foreach ($request->privileges as $privilege) {
                    $UP = new UserGroupPrivileges();
                    $UP->id_user_group = $User_group->id_user_group;
                    $UP->id_menu = $privilege['id_menu'];
                    $UP->has_view = $privilege['has_view'];
                    $UP->has_create = $privilege['has_create'];
                    $UP->has_edit = $privilege['has_edit'];
                    $UP->has_delete = $privilege['has_delete'];

                    $UP->save();
                }
            }

            return response()->json([
                'status' => 'success',
                'msg' => 'Berhasil menyimpan User Group'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'invalid parameter'
            ]);
        }
    }

    public function edit(Request $request)
    {
        if ($request->id_user_group && $request->name && $request->privileges) {
            $permission = PrivilegeHelper::getPermission();
            if (!$permission->has_edit) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
                ]);
            }

            // FIND USER_GROUP
            $user_group = UserGroups::find($request->id_user_group);
            if (!$user_group) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'User Group tidak ditemukan'
                ]);
            }

            // EDIT USER_GROUPS
            $user_group->name = $request->name;
            $user_group->updated_by = Auth::id();

            if ($user_group->save()) {

                // DELETE ALL PRIVILEGE
                UserGroupPrivileges::where('id_user_group', $request->id_user_group)->delete();

                // SAVE TO USER_GROUP_PRIVILEGES
                foreach ($request->privileges as $privilege) {
                    $UP = new UserGroupPrivileges();
                    $UP->id_user_group = $user_group->id_user_group;
                    $UP->id_menu = $privilege['id_menu'];
                    $UP->has_view = $privilege['has_view'];
                    $UP->has_create = $privilege['has_create'];
                    $UP->has_edit = $privilege['has_edit'];
                    $UP->has_delete = $privilege['has_delete'];

                    $UP->save();
                }
            }

            return response()->json([
                'status' => 'success',
                'msg' => 'Berhasil menyimpan User Group'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'invalid parameter'
            ]);
        }
    }

    public function delete(Request $request)
    {
        if (!$request->id) {
            return response()->json([
                'status' => 'error',
                'msg' => 'invalid parameter'
            ]);
        }

        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_delete) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $user_group = UserGroups::find($request->id);
        if (!$user_group) {
            return response()->json([
                'status' => 'error',
                'msg' => 'User Group tidak ditemukan'
            ]);
        }

        $user_group->deleted_by = Auth::id();
        $user_group->save();

        if ($user_group->delete()) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Berhasil menghapus User Group'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Terjadi kesalahan saat menghapus'
            ]);
        }
    }
}
