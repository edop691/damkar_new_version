<?php

namespace App\Http\Controllers;

use App\Helpers\PrivilegeHelper;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $permission = PrivilegeHelper::getPermission();
        if(!$permission->has_view){
            return abort(403);
        }

        return view('modules.dashboard.index');
    }
}
