<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="{{ env('APP_URL') }}">
            <span class="sidebar-brand-text align-middle">
                {{ env('APP_NAME') }}
                <sup><small class="badge bg-primary text-uppercase">Admin</small></sup>
            </span>
        </a>

        <ul class="sidebar-nav">

            {!! \App\Helpers\PrivilegeHelper::createMenu() !!}

        </ul>

    </div>
</nav>
