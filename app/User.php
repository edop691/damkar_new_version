<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Uuids, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'created_by', 'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'username'
    ];

    protected $primaryKey = 'id_user';

    public function user_group()
    {
        return $this->belongsTo('\App\UserGroups', 'id_user_group', 'id_user_group');
    }

    public function created_by()
    {
        return $this->belongsTo('\App\User', 'created_by', 'id_user');
    }

    public function updated_by()
    {
        return $this->belongsTo('\App\User', 'updated_by', 'id_user');
    }

    public function deleted_by()
    {
        return $this->belongsTo('\App\User', 'deleted_by', 'id_user');
    }
}
