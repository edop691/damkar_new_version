<?php

namespace App\Http\Controllers;

use App\Helpers\PrivilegeHelper;
use App\User;
use App\UserGroups;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return abort(403);
        }

        return view('modules.pengguna.index', ['permission' => $permission]);
    }

    public function all()
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $data = User::with([
            'user_group' => function ($query) {
                return $query->select('id_user_group', 'name');
            },
            'created_by' => function ($query) {
                return $query->select('id_user', 'name');
            },
            'updated_by'  => function ($query) {
                return $query->select('id_user', 'name');
            }
        ])->get();

        return Datatables::collection($data)->make(true);
    }

    public function master()
    {
        // GET MASTER SELECTION USER GROUP
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $user_groups = UserGroups::select('name', 'id_user_group')->get();
        return response()->json([
            'status' => 'success',
            'data' => $user_groups
        ]);
    }

    public function find(Request $request)
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_view) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $user = User::with('user_group:id_user_group,name')
            ->with('created_by:id_user,name')
            ->with('updated_by:id_user,name')
            ->find($request->id);

        // GET MASTER USER GROUP
        $user['master_user_group'] = UserGroups::select('name', 'id_user_group')->get();

        if ($user) {
            $user->makeVisible(['username']);
            return response()->json([
                'status' => 'success',
                'data' => $user
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Pengguna tidak ditemukan'
            ]);
        }
    }

    public function add(Request $request)
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_create) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $rules = [
            'nama_pengguna' => [
                'required',
                'max:255',
                'min:5'
            ],
            'id_user_group' => 'required',
            'username' => [
                'required',
                'max:255',
                'min:4',
                'unique:users,username'
            ],
            'password' => [
                'required',
                'min:5',
                'max:100'
            ],
            'email' => [
                'nullable',
                'max:255',
                'min:5',
                'email:rfc,dns',
                'unique:users,email'
            ],
            'telepon' => [
                'nullable',
                'min:8',
                'max:20',
                'regex:/(08)[0-9]/', // 2 digit pertama harus 08
                'unique:users,phone'
            ]
        ];

        $errorRules = [
            'nama_pengguna.required' => 'Nama pengguna tidak boleh kosong',
            'nama_pengguna.max' => 'Nama pengguna tidak boleh lebih dari :max karakter',
            'nama_pengguna.min' => 'Nama pengguna harus lebih dari :min karakter',

            'id_user_group.required' => 'Group pengguna harus dipilih',

            'username.required' => 'Username tidak boleh kosong',
            'username.max' => 'Username tidak boleh lebih dari :max karakter',
            'username.min' => 'Username harus lebih dari :min karakter',
            'username.unique' => 'Username sudah terdaftar, silahkan gunakan :attribute lain',

            'password.required' => 'Password tidak boleh kosong',
            'password.max' => 'Password tidak boleh lebih dari :max karakter',
            'password.min' => 'Password harus lebih dari :min karakter',

            'email.max' => 'Email tidak boleh lebih dari :max karakter',
            'email.min' => 'Email harus lebih dari :min karakter',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email sudah terdaftar, silahkan gunakan :attribute lain',

            'telepon.max' => 'Telepon tidak boleh lebih dari :max karakter',
            'telepon.min' => 'Telepon harus lebih dari :min karakter',
            'telepon.unique' => 'Telepon sudah terdaftar, silahkan gunakan :attribute lain',
            'telepon.regex' => 'Telepon tidak valid, gunakan format 08xxx',

        ];

        $validator = Validator::make($request->all(), $rules, $errorRules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->getMessageBag()
            ]);
        }

        $User = new User();
        $User->name = $request->nama_pengguna;
        $User->id_user_group = $request->id_user_group;
        $User->username = $request->username;
        $User->password = Hash::make($request->password);
        $User->email = $request->email;
        $User->phone = $request->telepon;
        $User->created_by = Auth::id();

        if ($User->save()) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Berhasil menyimpan pengguna'
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'Terjadi kesalahan pada saat menyimpan pengguna'
        ]);
    }

    public function edit(Request $request)
    {
        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_create) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $rules = [
            'id_user' => [
                'required'
            ],
            'nama_pengguna' => [
                'required',
                'max:255',
                'min:5'
            ],
            'id_user_group' => 'required',
            'username' => [
                'required',
                'max:255',
                'min:4',
                'unique:users,username,' . $request->id_user .',id_user'
            ],
            'password' => [
                'nullable',
                'min:5',
                'max:100'
            ],
            'email' => [
                'nullable',
                'max:255',
                'min:5',
                'email:rfc,dns',
                'unique:users,email,' . $request->id_user . ',id_user'
            ],
            'telepon' => [
                'nullable',
                'min:8',
                'max:20',
                'regex:/(08)[0-9]/', // 2 digit pertama harus 08
                'unique:users,phone,' . $request->id_user . ',id_user'
            ]
        ];

        $errorRules = [

            'id_user.required' => 'Missing Parameter, silahkan reload halaman dan coba lagi',

            'nama_pengguna.required' => 'Nama pengguna tidak boleh kosong',
            'nama_pengguna.max' => 'Nama pengguna tidak boleh lebih dari :max karakter',
            'nama_pengguna.min' => 'Nama pengguna harus lebih dari :min karakter',

            'id_user_group.required' => 'Group pengguna harus dipilih',

            'username.required' => 'Username tidak boleh kosong',
            'username.max' => 'Username tidak boleh lebih dari :max karakter',
            'username.min' => 'Username harus lebih dari :min karakter',
            'username.unique' => 'Username sudah terdaftar, silahkan gunakan :attribute lain',

            'password.required' => 'Password tidak boleh kosong',
            'password.max' => 'Password tidak boleh lebih dari :max karakter',
            'password.min' => 'Password harus lebih dari :min karakter',

            'email.max' => 'Email tidak boleh lebih dari :max karakter',
            'email.min' => 'Email harus lebih dari :min karakter',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email sudah terdaftar, silahkan gunakan :attribute lain',

            'telepon.max' => 'Telepon tidak boleh lebih dari :max karakter',
            'telepon.min' => 'Telepon harus lebih dari :min karakter',
            'telepon.unique' => 'Telepon sudah terdaftar, silahkan gunakan :attribute lain',
            'telepon.regex' => 'Telepon tidak valid, gunakan format 08xxx',

        ];

        $validator = Validator::make($request->all(), $rules, $errorRules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->getMessageBag()
            ]);
        }

        $user = User::find($request->id_user);
        if(!$user){
            return response()->json([
                'status' => 'error',
                'msg' => 'Pengguna tidak ditemukan'
            ]);
        }

        $user->name = $request->nama_pengguna;
        $user->id_user_group = $request->id_user_group;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->phone = $request->telepon;
        $user->updated_by = Auth::id();

        // update password jika field password diisi
        if($request->password){
            $user->password = Hash::make($request->password);
        }

        if ($user->save()) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Berhasil menyimpan pengguna'
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'Terjadi kesalahan pada saat menyimpan pengguna'
        ]);
    }

    public function delete(Request $request)
    {
        if (!$request->id) {
            return response()->json([
                'status' => 'error',
                'msg' => 'invalid parameter'
            ]);
        }

        $permission = PrivilegeHelper::getPermission();
        if (!$permission->has_delete) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Anda tidak memiliki izin untuk melakukan operasi ini'
            ]);
        }

        $user = User::find($request->id);

        if (!$user) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Pengguna tidak ditemukan'
            ]);
        }

        $user->deleted_by = Auth::id();
        $user->save();

        if ($user->delete()) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Berhasil menghapus Pengguna'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Terjadi kesalahan saat menghapus'
            ]);
        }
    }
}
