<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanAkhirPelaporTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_akhir_pelapor', function (Blueprint $table) {
            $table->id('id_laporan_akhir_pelapor');
            $table->string('alat')->nullable();
            $table->string('jenis_penyelamatan')->nullable();
            $table->string('unit_kendaraan')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('nama_pemilik')->nullable();
            $table->string('sumber_api')->nullable();
            $table->string('object_terbakar')->nullable();
            $table->integer('korban_jiwa')->nullable();
            $table->string('regu')->nullable();
            $table->string('catatan_khusus', 3000)->nullable();
            $table->string('status')->nullable();
            $table->string('waktu_kejadian')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_akhir_pelapor');
    }
}
