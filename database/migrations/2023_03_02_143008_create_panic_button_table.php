<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanicButtonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aduan_panic_button', function (Blueprint $table) {
            $table->id('id_aduan_panic_button');
            $table->string('no_aduan_panic_button')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->integer('kelurahan')->nullable();
            $table->integer('kecamatan')->nullable();
            $table->string('foto')->nullable();
            $table->string('foto_path')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('jam')->nullable();
            $table->string('jenis_kedaruratan')->nullable();
            $table->string('catatan_khusus')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aduan_panic_button');
    }
}
