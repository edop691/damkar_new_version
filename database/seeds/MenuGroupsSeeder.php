<?php

use App\MenuGroups;
use Illuminate\Database\Seeder;

class MenuGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Management',
                'sort' => 1
            ],
            [
                'name' => 'Laporan',
                'sort' => 2
            ]
        ];

        foreach ($data as $item) {
            $Data = new MenuGroups();
            $Data->name = $item['name'];
            $Data->sort = $item['sort'];

            $Data->save();
        }
    }
}
