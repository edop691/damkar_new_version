@extends('layouts.dashboard')

@section('title', 'Management Group Pengguna')

@push('css')
@endpush

@section('content')
    <div class="container-fluid p-0">

        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3><strong>Management</strong> Group Pengguna</h3>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="card-actions float-end">
                    @if($permission->has_create)
                    <button class="btn btn-primary btn-sm btn-pill" onclick="addData()">
                        Tambah Data
                    </button>
                    @endif
                </div>
                <h5 class="card-title">Daftar Group Pengguna</h5>
            </div>
            <div class="card-body">
                <table id="table_user_group" class="table table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Dibuat Pada</th>
                            <th>Dibuat Oleh</th>
                            <th>Diperbarui Pada</th>
                            <th>Diperbarui Oleh</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    @if ($permission->has_view)
        @include('modules.user_group.modal_view')
    @endif

    @if ($permission->has_create)
        @include('modules.user_group.modal_add')
    @endif

    @if ($permission->has_edit)
        @include('modules.user_group.modal_edit')
    @endif

    @if ($permission->has_delete)
        @include('modules.user_group.modal_delete')
    @endif
@endsection

@push('js')
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/bootstrap-maxlength.js') }}"></script>
    @stack('component_js')
    <script>
        var TBL_USER_GROUP;
        $(document).ready(function() {
            TBL_USER_GROUP = $("#table_user_group").DataTable({
                responsive: true,
                language: {
                    "emptyTable": "Tidak Ada Data",
                    "lengthMenu": "Menampilkan _MENU_ Data/Halaman",
                    "zeroRecords": "Tidak Ada Data",
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak Ada Data",
                    "infoFiltered": "(memfilter data dari _MAX_ total total data)",
                    "search": "Cari Group Pengguna: ",
                    "processing": "Sedang memproses...",
                    "paginate": {
                        "first": "Pertama",
                        "last": "Terakhir",
                        "next": "Selanjutnya",
                        "previous": "Sebelumnya"
                    },
                },
                processing: true,
                serverSide: true,
                ajax: "{{ env('APP_URL') }}/user-group/all",
                error: function(e) {
                    console.log(e);
                },
                columns: [{
                        data: 'name',
                    },
                    {
                        data: 'created_at',
                        render: function(data) {
                            if (data !== null && data !== "") {
                                return moment(data).format('DD/MM/YYYY')
                            } else {
                                return "-";
                            }
                        }
                    },
                    {
                        data: 'created_by',
                        render: function(data) {
                            if (data !== null && data !== "") {
                                if (data.hasOwnProperty('name')) {
                                    return data.name;
                                } else {
                                    return '-';
                                }
                            } else {
                                return '-';
                            }
                        }
                    },
                    {
                        data: 'updated_at',
                        render: function(data) {
                            if (data !== null && data !== "") {
                                return moment(data).format('DD/MM/YYYY')
                            } else {
                                return "-";
                            }
                        }
                    },
                    {
                        data: 'updated_by',
                        render: function(data) {
                            if (data !== null && data !== "") {
                                if (data.hasOwnProperty('name')) {
                                    return data.name;
                                } else {
                                    return '-';
                                }
                            } else {
                                return '-';
                            }
                        }
                    },
                    {
                        data: 'id_user_group',
                        render: function(data) {
                            return `
                                @if ($permission->has_view)
                                <button onclick="viewData('${data}')" class="btn btn-sm btn-outline-primary">
                                    <i data-feather="eye"></i>
                                </button>
                                @endif

                                @if ($permission->has_edit)
                                <button onclick="editData('${data}')" class="btn btn-sm btn-outline-warning">
                                    <i data-feather="edit-2"></i>
                                </button>
                                @endif

                                @if ($permission->has_delete)
                                <button onclick="deleteData('${data}')" class="btn btn-sm btn-outline-danger">
                                    <i data-feather="trash-2"></i>
                                </button>
                                @endif
                            `;
                        }
                    },
                ]
            });

            $('#table_user_group').on('draw.dt', function() {
                feather.replace()
            });
        });
    </script>
@endpush
