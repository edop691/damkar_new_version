<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendataanSaranaPerusahaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendataan_sarana_perusahaan', function (Blueprint $table) {
            $table->id('id_sarana_peralatan');
            $table->string('id_pendataan_umum')->nullable();
            $table->string('alarm')->nullable();
            $table->string('fungsi_alarm')->nullable();
            $table->string('fungsi_alarm_baik')->nullable();
            $table->string('jenis_proteksi_kebakaran')->nullable();
            $table->string('jml_jenis_proteksi_kebakaran')->nullable();
            $table->string('hydrant')->nullable();
            $table->string('jml_hydrant')->nullable();
            $table->string('sumber_air_hydrant')->nullable();
            $table->string('volume_tangki_hydrant')->nullable();
            $table->string('hydrant_tertutup')->nullable();
            $table->string('bahan_produksi')->nullable();
            $table->string('apar')->nullable();
            $table->string('jarak_apar')->nullable();
            $table->string('jenis_apar')->nullable();
            $table->string('kondisi_apar')->nullable();
            $table->string('id_jenis_apar')->nullable();
            $table->string('catatan')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendataan_sarana_perusahaan');
    }
}
