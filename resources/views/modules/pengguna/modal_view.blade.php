<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Pengguna</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body m-3">
                <div class="loader">
                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                    <div class="form-group placeholder-glow mb-2">
                        <div class="placeholder col-4 placeholder-sm"></div>
                        <div class="placeholder col-12 placeholder-lg"></div>
                    </div>

                </div>

                <div class="data"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

@push('component_js')
    <script>
        const MODAL_VIEW = new bootstrap.Modal($('#modal_view'));

        function viewData(id) {
            $('#modal_view .data').hide();
            $('#modal_view .loader').fadeIn();

            $("#modal_view .data").html('');

            MODAL_VIEW.show();

            $.ajax({
                url: '{{ env('APP_URL') }}/pengguna/' + id,
                method: 'GET',
                success: function(response) {
                    if (response.status == 'success') {
                        let data = response.data;
                        $('#modal_view .data').html(`
                            <div class="mb-3">
                                <label class="from-label">Nama Pengguna</label>
                                <input class="form-control" type="text" disabled value="${data.name}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Username</label>
                                <input class="form-control" type="text" disabled value="${data.username}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Email</label>
                                <input class="form-control" type="text" disabled value="${data.email ? data.email : '-'}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Telepon</label>
                                <input class="form-control" type="tel" disabled value="${data.phone ? data.phone : '-'}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">User Group</label>
                                <input class="form-control" type="text" disabled value="${data.user_group.name}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Dibuat Oleh</label>
                                <input class="form-control" type="text" disabled value="${data.created_by?.name ? data.created_by.name : '-'}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Dibuat Pada</label>
                                <input class="form-control" type="text" disabled value="${moment(data.created_at).format('DD/MM/YYYY')}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Diperbarui Oleh</label>
                                <input class="form-control" type="text" disabled value="${data.updated_by?.name ? data.updated_by.name : '-'}">
                            </div>

                            <div class="mb-3">
                                <label class="from-label">Diperbarui Pada</label>
                                <input class="form-control" type="text" disabled value="${moment(data.updated_at).format('DD/MM/YYYY')}">
                            </div>
                        `);

                        $('#modal_view .data').fadeIn();
                        $('#modal_view .loader').hide();
                    } else {
                        MODAL_VIEW.hide();
                        if (TBL_USER) {
                            TBL_USER.ajax.reload(null, false);
                        }
                        window.notyf.open({
                            type: response.status,
                            message: response.msg,
                            duration: 10000,
                            ripple: true,
                            dismissible: true,
                            position: {
                                x: "right",
                                y: "top"
                            }
                        });
                    }
                },
                error: function() {
                    MODAL_VIEW.hide();
                    if (TBL_USER) {
                        TBL_USER.ajax.reload(null, false);
                    }
                    window.notyf.open({
                        type: "error",
                        message: "Internal Server Error",
                        duration: 10000,
                        ripple: true,
                        dismissible: true,
                        position: {
                            x: "right",
                            y: "top"
                        }
                    });
                }
            })

        }

        $('#modal_view').on('hidden.bs.modal', function() {
            $("#modal_view .data").html('');
        });
    </script>
@endpush
