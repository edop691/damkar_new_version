<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendataanUmumPerusahaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendataan_umum_perusahaan', function (Blueprint $table) {
            $table->id('id_pendataan_umum');
            $table->string('nama_pendataan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('telepon')->nullable();
            $table->string('pemilik')->nullable();
            $table->string('alamat_pemilik')->nullable();
            $table->string('luas_persil')->nullable();
            $table->string('luas_bangunan')->nullable();
            $table->string('jumlah_karyawan')->nullable();
            $table->string('penggunaan_bangunan')->nullable();
            $table->string('no_izin_bangunan')->nullable();
            $table->string('sumber_listrik')->nullable();
            $table->string('ket_sumber_listrik')->nullable();
            $table->string('penangkal_petir')->nullable();
            $table->string('sumber_air')->nullable();
            $table->string('ket_sumber_air', 3000)->nullable();
            $table->string('riwayat_kebakaran')->nullable();
            $table->string('waktu_kebakaran')->nullable();
            $table->string('kerugian_jiwa')->nullable();
            $table->string('kerugian_material')->nullable();
            $table->string('asuransi_kebakaran')->nullable();
            $table->string('tempat_asuransi')->nullable();
            $table->string('tahun_asuransi')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendataan_umum_perusahaan');
    }
}
