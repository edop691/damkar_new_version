<?php

use App\UserGroups;
use Illuminate\Database\Seeder;

class UserGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Data = new UserGroups();
        $Data->name = 'Superuser';
        $Data->save();
    }
}
