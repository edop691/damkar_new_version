<?php

use App\Menus;
use App\UserGroupPrivileges;
use App\UserGroups;
use Illuminate\Database\Seeder;

class UserGroupPrivilegesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // GET USER GROUP SUPERUSER
        $user_group = UserGroups::where('name', 'Superuser')->first();

        // GET MENUS
        $menus = Menus::all();

        // SET ACCESS TO ALL MENU
        foreach ($menus as $item) {
            $Data = new UserGroupPrivileges();
            $Data->id_user_group = $user_group->id_user_group;
            $Data->id_menu = $item->id_menu;
            $Data->has_view = 1;
            $Data->has_create = 1;
            $Data->has_edit = 1;
            $Data->has_delete = 1;

            $Data->save();
        }
    }
}
