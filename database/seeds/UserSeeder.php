<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\UserGroups;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_group = UserGroups::where('name', 'Superuser')->first();

        $User = new User();
        $User->name = 'Administrator';
        $User->username = 'root';
        $User->id_user_group = $user_group->id_user_group;
        $User->password = Hash::make('root');
        if ($User->save()) {
            $User->created_by = $User->id_user;
            $User->save();
        }

        // INSERT CREATED_BY
        $user_groups = UserGroups::all();
        foreach ($user_groups as $item) {
            $item->created_by = $User->id_user;
            $item->save();
        }
    }
}
