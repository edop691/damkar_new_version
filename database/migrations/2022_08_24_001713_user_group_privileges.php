<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserGroupPrivileges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_group_privileges', function (Blueprint $table) {
            $table->uuid('id_user_group_privilege')->primary();
            $table->uuid('id_user_group');
            $table->uuid('id_menu');
            $table->smallInteger('has_view')->default(0);
            $table->smallInteger('has_create')->default(0);
            $table->smallInteger('has_edit')->default(0);
            $table->smallInteger('has_delete')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_group_privileges');
    }
}
