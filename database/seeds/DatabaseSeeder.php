<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MenuGroupsSeeder::class,
            MenusSeeder::class,
            UserGroupsSeeder::class,
            UserGroupPrivilegesSeeder::class,
            UserSeeder::class,
        ]);
    }
}
