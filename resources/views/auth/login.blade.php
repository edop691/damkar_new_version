@extends('layouts.auth')
@section('title', 'Login')
@section('content')
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">

                    
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mt-4">
                                <img src="{{ asset('img/photos/logo_damkar.PNG') }}" width="300" alt="">
                                <h1><strong>Dinas Pemadam Kebakaran Kab. Bogor</strong></h1>
                                <p class="lead">
                                    
                                </p>
                            </div>
                            @if (session('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                    <div class="alert-icon">
                                        <i class="far fa-fw fa-bell"></i>
                                    </div>
                                    <div class="alert-message">
                                        <strong>Error!</strong> {{ session('error') }}
                                    </div>
                                </div>
                            @endif
                            <div class="m-sm-4">
                                <form method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label">Username</label>
                                        <input class="form-control form-control-lg threshold" type="text" name="username"
                                            placeholder="Username" maxlength="255" />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Password</label>
                                        <input class="form-control form-control-lg threshold" type="password"
                                            name="password" placeholder="Password" maxlength="50" />
                                        <small>
                                            <a href="#">Forgot password?</a>
                                        </small>
                                    </div>
                                    <div>
                                        <label class="form-check">
                                            <input class="form-check-input" type="checkbox" value="true" name="remember"
                                                checked>
                                            <span class="form-check-label">
                                                Ingat saya
                                            </span>
                                        </label>
                                    </div>
                                    <div class="text-center mt-3">
                                        <button type="submit" class="btn btn-lg btn-primary">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-maxlength.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.threshold').maxlength({
                alwaysShow: true,
                warningClass: "badge bg-primary",
                limitReachedClass: "badge bg-danger"
            });
        })
    </script>
@endsection
