@extends('layouts.dashboard')

@section('title', 'Dashboard')

@section('content')
    <div class="container-fluid p-0">

        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3><strong>Dashboard</strong> Analinis</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-12 col-xxl-12 d-flex">
                <div class="card flex-fill">
                    <div class="card-body">
                        <iframe src="https://mitratechid.maps.arcgis.com/apps/dashboards/d09088dd1b644fd2a4691eaaba1af422" frameborder="0" width="100%" height="800"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var chart_tamu;
        document.addEventListener("DOMContentLoaded", function() {
            var ctx = document.getElementById("chartjs-dashboard-line").getContext("2d");
            var gradientLight = ctx.createLinearGradient(0, 0, 0, 225);
            gradientLight.addColorStop(0, "rgba(215, 227, 244, 1)");
            gradientLight.addColorStop(1, "rgba(215, 227, 244, 0)");
            var gradientDark = ctx.createLinearGradient(0, 0, 0, 225);
            gradientDark.addColorStop(0, "rgba(51, 66, 84, 1)");
            gradientDark.addColorStop(1, "rgba(51, 66, 84, 0)");

            // GENERATE MOCKUP DATA
            let current_tanggal = "{{ date('d') }}";
            let labels = [];
            let data = [];
            for (let i = 1; i <= current_tanggal; i++) {
                labels.push(i);
                // Returns a random integer from 1 to 30:
                data.push(Math.floor(Math.random() * 30) + 1);
            }

            // Line chart
            chart_tamu = new Chart(document.getElementById("chartjs-dashboard-line"), {
                type: "line",
                data: {
                    labels: labels,
                    datasets: [{
                        fill: true,
                        backgroundColor: window.theme.id === "light" ? gradientLight : gradientDark,
                        borderColor: window.theme.primary,
                        data: data
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        intersect: false
                    },
                    hover: {
                        intersect: true
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            reverse: true,
                            gridLines: {
                                color: "rgba(0,0,0,0.0)"
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 1000
                            },
                            display: true,
                            borderDash: [3, 3],
                            gridLines: {
                                color: "rgba(0,0,0,0.0)",
                                fontColor: "#fff"
                            }
                        }]
                    }
                }
            });
        });
    </script>
    <script src="{{ asset('js/moment.js') }}"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            let date = new Date();
            let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            flatpickr(".flatpickr-range", {
                mode: "range",
                dateFormat: "d-m-Y",
                maxDate: 'today',
                disableMobile: "true",
                defaultDate: [firstDay, '{{ \Carbon\Carbon::now('Asia/Jakarta')->format('d-m-Y') }}'],
                onChange: function(selectedDates, dateStr, instance) {
                    if (selectedDates.length > 1) {
                        let start = moment(selectedDates[0]);
                        let end = moment(selectedDates[1]);

                        console.log({
                            start: start.format('DD-MM-YYYY'),
                            end: end.format('DD-MM-YYYY')
                        })

                        // TODO: HIT API
                    }
                },
            });
        })
    </script>
@endpush
